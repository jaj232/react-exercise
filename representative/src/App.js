import React from 'react';
import styled from 'styled-components';

import Main from './components/Main';

const AppContainer = styled.div`
  margin: 5%;
  border: 4px ridge grey;
  border-radius: 4px;
  padding: 10px;
`;

function App() {
  return (
    <AppContainer>
      <Main></Main>
    </AppContainer>
  );
}

export default App;
