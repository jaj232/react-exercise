import React from 'react';
import _ from 'lodash';
import styled from 'styled-components';
import TextField from '@material-ui/core/TextField';

const InfoText = styled.div`
  font-size: 24px;
  margin-bottom: 20px;
`;
const StyledTextField = styled(TextField)`
  margin-bottom: 20px !important;
`;

class Info extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const nameParts = _.get(this.props, 'info.name', '').split(' ');
    const firstName = _.get(nameParts, '[0]', '');
    const lastName = _.get(nameParts, '[1]', '');// Assuming only 2 names

    const district = _.get(this.props, 'info.district', '');
    const phone = _.get(this.props, 'info.phone', '');
    const office = _.get(this.props, 'info.office', '');

    return (
      <div>
        <InfoText>Info</InfoText>
        <div>
          <StyledTextField label="First Name" value={firstName} variant="filled" disabled fullWidth />
          <StyledTextField label="Last Name" value={lastName} variant="filled" disabled fullWidth />
          <StyledTextField label="District" value={district} variant="filled" disabled fullWidth />
          <StyledTextField label="Phone" value={phone} variant="filled" disabled fullWidth />
          <StyledTextField label="Office" value={office} variant="filled" disabled fullWidth />
        </div>
      </div>
    );
  }

}

export default Info;