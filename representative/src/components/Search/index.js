import React from 'react';
import styled from 'styled-components';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import StatesOptions from './States';

const SearchContainer = styled.div`
  display: grid;
  grid-template-columns: auto auto auto;
  grid-gap: 15%;
`;

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      type: 'representatives',
      usState: 'AL'
    };
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value});
  }

  handleSearch = () => {
    this.props.onSearch(this.state);
  }

  render() {
    return (
      <SearchContainer>
        <FormControl>
          <InputLabel htmlFor="select-type">Type</InputLabel>
          <Select id='select-type' name='type' value={this.state.type} onChange={this.handleChange.bind(this)} native>
            <option value="representatives">Representative</option>
            <option value="senators">Senator</option>
          </Select>
        </FormControl>

        <FormControl>
          <InputLabel htmlFor="select-state">State</InputLabel>
          <Select id='select-state' name='usState' value={this.state.usState} onChange={this.handleChange.bind(this)} native>
            <StatesOptions/>
          </Select>
        </FormControl>
        
        <Button onClick={this.handleSearch} variant="contained" color="primary">Search</Button>
      </SearchContainer>
    );
  }
}

export default Search;