import React from 'react';
import styled from 'styled-components';
import Snackbar from '@material-ui/core/Snackbar';

import Search from '../Search';
import List from '../List';
import Info from '../Info';

const Header = styled.h1`
  color: #2e9fff;
`;
const SearchResultsArea = styled.div`
  margin-top: 30px;
  display: grid;
  grid-template-columns: 48% 48%;
  grid-gap: 4%;
`;

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      type: '',
      currentInfo: {},
      showError: false,
      errorMessage: ''
    };
  }

  handleSearch = async(searchParameters) => {
    if (!searchParameters.type || !searchParameters.usState) return;

    const response = await fetch(`http://localhost:3000/${searchParameters.type}/${searchParameters.usState}`);
    const jsonResponse = await response.json();
    if (!jsonResponse.success) {
      this.setState({ list: [], type: '', currentInfo: {}, showError: true, errorMessage: jsonResponse.error });
      return;
    }

    this.setState({ list: jsonResponse.results, type: searchParameters.type });
  }

  handleRowClick = (info) => {
    this.setState({ currentInfo: info});
  }

  handleClose = (event, reason) => {
    if (reason === 'clickaway') return;
    this.setState({ showError: false, errorMessage: '' });
  };

  render() {
    return (
      <div>
        <Header>Who's My Representative?</Header>
        <Search onSearch={this.handleSearch} />
        <SearchResultsArea>
          <List list={this.state.list} type={this.state.type} onRowClick={this.handleRowClick}></List>
          <Info info={this.state.currentInfo}></Info>
        </SearchResultsArea>
        <Snackbar 
          open={this.state.showError} 
          autoHideDuration={3000} 
          onClose={this.handleClose} 
          message={this.state.errorMessage}
        />
      </div>
    );
  }
}

export default Main;