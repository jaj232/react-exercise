import React from 'react';
import MaterialTable from 'material-table';
import styled from 'styled-components';

const ListText = styled.div`
  text-transform: capitalize;
  font-size: 24px;
`;
const EmphasizedText = styled.span`
  color: #2e9fff;
`;
const AbbreviatedText = styled.p`
  visibility: hidden;
  &::first-letter {
    visibility: visible;
  }
`;

class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        { title: 'Name', field: 'name', cellStyle: {padding: '0px 16px'} },
        { title: 'Party', field: 'party', cellStyle: {padding: '0px 16px'}, render: rowData => <AbbreviatedText>{rowData.party}</AbbreviatedText> }
      ]
    };
  }

  handleRowClick = (event, RowData) => {
    this.props.onRowClick(RowData);
  }

  render() {
    return (
      <div>
        <MaterialTable
          title={
            <ListText>
              List / <EmphasizedText>{this.props.type}</EmphasizedText>
            </ListText>
          }
          columns={this.state.columns}
          data={this.props.list}
          onRowClick={this.handleRowClick}
          options={{
            headerStyle: {
              backgroundColor: '#EEEEEE'
            }
          }}
        />
      </div>
    );
  }

}

export default List;